 var cheerio = require('cheerio');
//var request = require('request');

module.exports = function(pagina, callback){
        var $ = cheerio.load(pagina);
        var giorno = $('.active', '#daytabs').children().children().first().text().trim();
        console.log('giorno =' + giorno);  // => "DAY NUM"
        var tmin = $('.active', '#daytabs').find('.tmin').text();
        //console.log('tmin ='+tmin);// => 16°
        var tmax = $('.active', '#daytabs').find('.tmax').text();
        //console.log('tmax ='+tmax);// => 21°
        var temp = {
          min: tmin,
          max: tmax
        };
        callback(temp);
        return 0;
}
