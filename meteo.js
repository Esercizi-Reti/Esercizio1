const getTemp = require('./Modules/getTemp')
const https = require('https');
const http = require('http');

console.log('Starting service...');

//set parameters
var dbHost = 'localhost';
var dbPort = '5984';
var dbPath = '/meteo';

var options = {
  hostname : 'www.ilmeteo.it',
  method : 'GET',
  path: '/meteo/roma'
};

var client = https.request(options, (result)=>{
        result.setEncoding('utf8');
        console.log(result.statusCode);
        var page ='';
        result.on('data', (chunk)=>{
          page+=chunk;
        });
        result.on('end', ()=>{
          // console.log(page);
          getTemp(page, (result)=>{
            console.log(result);
            var temp = result;
            var couchOptions = {
              hostname : dbHost,
              port : dbPort,
              path : dbPath,
              method : 'POST',
              headers: {
                  'Content-type': 'application/json',
                  'Content-length': Buffer.byteLength(JSON.stringify(temp))
              }
            };
            var couch = http.request(couchOptions, (result)=>{
              result.setEncoding('utf8');
              console.log(result.statusCode);
            });
            couch.write(JSON.stringify(temp));
            couch.end();
          });
        });
});
client.end();
